module test

go 1.13

require (
	github.com/ethereum/go-ethereum v1.9.11
	github.com/gin-gonic/gin v1.5.0
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/sessions v1.2.0
)

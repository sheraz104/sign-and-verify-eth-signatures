package main

import (
	"bytes"
	"crypto/ecdsa"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
)

func main()  {
	privateKey, err := crypto.HexToECDSA(
		"F8ADC41546F9C63E057DD345B5D81ECE746656ECDE90B9DBD9BDB7B2B2662913",
	)
	checkError(err)
	
	publicKey := privateKey.Public()
	publicKeyECDSA := publicKey.(*ecdsa.PublicKey)
	publicKeyBytes := crypto.FromECDSAPub(publicKeyECDSA)

	hash, sig := generateSig(privateKey)
	valid := validateSig(
		hash,
		sig,
		publicKeyBytes,
	)
	if valid {
		fmt.Println("matches")
	} else {
		fmt.Println("doesn't match")
	}
}

func generateSig(privKey *ecdsa.PrivateKey) (common.Hash, []byte) {
	data := []byte("Hello, world")
	hash := crypto.Keccak256Hash(data)
	signature, err := crypto.Sign(
		hash.Bytes(),
		privKey,
	)
	checkError(err)
	return hash, signature
}

func validateSig(hash common.Hash, sig []byte, publicKeyBytes []byte) bool {
	publicKeyRecovered, err := crypto.Ecrecover(
		hash.Bytes(),
		sig,
	)
	checkError(err)

	return bytes.Equal(
		publicKeyRecovered,
		publicKeyBytes,
	)
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}